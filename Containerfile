#
# Container image for jekyll-theme-centos theme.
#
FROM docker.io/library/ruby:3.2.2
MAINTAINER Alain Reguera Delgado <areguera@centosproject.org>

ENV BUNDLE_APP_CONFIG=/srv/jekyll-theme-centos/.bundle

WORKDIR /srv/jekyll-theme-centos

# ------------------------------------------------------------------------------
# jekyll-theme-centos theme configuration
#
# - The `Gemfile' is configured with `path:' to read the theme files from
# source. When you use this container image to render your sites, you can omit
# the theme version inside the _config.yml file. In such case, the site will be
# rendered using the theme version installed from source in this container
# image. See the contaier image tag to know what them version are you using.
#
# - The `Gemfile.lock' provides all the theme dependencies this them needs to
# render sites. They are not downloaded each time you render a site,
# consequently, the rendering process is significantly faster.
# ------------------------------------------------------------------------------
ADD .bundle ./.bundle
RUN bundle config set --local path /srv/jekyll-theme-centos/.bundle
ADD Gemfile .
ADD Gemfile.lock .
ADD src ./src
RUN bundle add jekyll-theme-centos --path /srv/jekyll-theme-centos/src

# ------------------------------------------------------------------------------
# `jq': command-line to process JSON documents. Useful to parse output from
# GitLab API requests. For example, to retrieve the last release number of a
# component.
# ------------------------------------------------------------------------------
RUN apt-get update && apt-get install -y jq

# ------------------------------------------------------------------------------
# `yq': command-line to process YAML documents. Useful to update .gitlab-ci.yml
# file when new component versions are released.
# ------------------------------------------------------------------------------
RUN wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O /usr/bin/yq && chmod +x /usr/bin/yq
